import uuid

from application.config import db, app
from application.models import Page, Repo
from .model.question import Question
from .model.question_mapper import QuestionMapper
from .model.question_model import QuestionModel

class QuestionFilters:
    def __init__(self, author_id):
        self.author_id = author_id

class QuestionsService:
    def __init__(self):
        self.questions_mapper = QuestionMapper()
        self.repo = Repo[QuestionModel](QuestionModel)

    def create_question(self, request_data) -> Question:
        question = self.questions_mapper.map_request(request_data)

        entity = QuestionModel(
            id=question.id,
            author_id=question.author_id,
            body=question.body
        )
        entity = self.repo.save_entity(entity)
        return self.questions_mapper.map_entity_to_dto(entity)

    def update_question(self, question_id, request_data) -> Question:
        question = self.questions_mapper.map_request(request_data)
        entity = self.repo.get_entity(question_id)
        entity = self.questions_mapper.assign_dto_to_entity(question, entity)
        entity = self.repo.save_entity(entity)
        return self.questions_mapper.map_entity_to_dto(entity)

    def get_question(self, question_id) -> Question:
        entity = self.repo.get_entity(question_id)
        return self.questions_mapper.map_entity_to_dto(entity)

    def get_questions(self, filters: QuestionFilters, page: int, size: int) -> Page[Question]:
        paginated_query = QuestionModel.query.paginate(page=page, per_page=size, error_out=False)
        entities = paginated_query.items
        questions = [self.questions_mapper.map_entity_to_dto(entity) for entity in entities]
        return Page(size=size, page=page, total_pages=paginated_query.pages, content=questions)
