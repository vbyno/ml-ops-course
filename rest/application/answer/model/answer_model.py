import uuid
import arrow
from sqlalchemy_utils import ArrowType
from sqlalchemy.dialects.postgresql import UUID

from application.config import db

class AnswerModel(db.Model):
    __tablename__ = 'answers'

    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    author_id = db.Column(UUID(as_uuid=True), nullable=False)
    question_id = db.Column(UUID(as_uuid=True), nullable=False)
    body = db.Column(db.String, nullable=False)
    score = db.Column(db.Integer, nullable=False, default=0)
    created_at = db.Column(ArrowType, nullable=False, default=arrow.utcnow())

    def __repr__(self):
        return f'<Answer {self.id}>'
