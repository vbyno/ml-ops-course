import uuid

from application.models import Page, Repo
from .model.answer import Answer
from .model.answer_model import AnswerModel
from .model.answer_mapper import AnswerMapper

class AnswerFilters:
    def __init__(self, author_id, question_id):
        self.author_id = author_id
        self.question_id = question_id

class AnswersService:
    def __init__(self):
        self.answer_mapper = AnswerMapper()
        self.repo = Repo[AnswerModel](AnswerModel)

    def create_answer(self, request_data) -> Answer:
        answer = self.answer_mapper.map_request(request_data)

        entity = AnswerModel(
            id=answer.id,
            author_id=answer.author_id,
            question_id=answer.question_id,
            score=answer.score,
            body=answer.body
        )
        entity = self.repo.save_entity(entity)
        return self.answer_mapper.map_entity_to_dto(entity)

    def update_answer(self, answer_id, request_data) -> Answer:
        answer = self.answer_mapper.map_request(request_data)
        entity = self.repo.get_entity(answer_id)
        entity = self.answer_mapper.assign_dto_attributes_to_entity(answer, entity)
        entity = self.repo.save_entity(entity)
        return self.answer_mapper.map_entity_to_dto(entity)

    def get_answer(self, answer_id) -> Answer:
        entity = self.repo.get_entity(answer_id)
        return self.answer_mapper.map_entity_to_dto(entity)

    def get_answers(self, filters: AnswerFilters, page: int, size: int) -> Page[Answer]:
        paginated_query = AnswerModel.query.paginate(page=page, per_page=size, error_out=False)
        entities = paginated_query.items
        questions = [self.answer_mapper.map_entity_to_dto(entity) for entity in entities]
        return Page(size=size, page=page, total_pages=paginated_query.pages, content=questions)
