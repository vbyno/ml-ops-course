from application.question.questions_service import QuestionsService
from application.answer.answers_service import AnswersService
from application.author import AuthorModel

questions_service  = QuestionsService()
answers_service = AnswersService()

import application.question.questions_controller
import application.answer.answers_controller

# from .config import db, app
