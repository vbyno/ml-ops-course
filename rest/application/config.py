import os
from dotenv import load_dotenv
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app_env=os.environ.get('APP_ENV', 'development')

dotenv_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir, os.pardir)
env_files = ['.env', '.env.local', f'.env.{app_env}', f'.env.{app_env}.local']

for file in env_files:
    load_dotenv(dotenv_path=os.path.join(dotenv_path, file), override=True, verbose=True)

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URL')
    db.init_app(app)
    return app

app = create_app()
migrate = Migrate(app, db)
