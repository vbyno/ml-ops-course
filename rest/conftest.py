import pytest
from application.config import create_app

@pytest.fixture(scope='module', autouse=True)
def app_context():
    app = create_app()
    with app.app_context():
        yield app
