import os

from application.config import app

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = os.getenv('APP_PORT'))
