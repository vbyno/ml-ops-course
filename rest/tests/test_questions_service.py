import uuid

from application.models import Page
from application.question.questions_service import QuestionsService, QuestionFilters
from application.question.model.question import Question

def _create_question(data):
    defaults = { "id": uuid.uuid4(), "author_id": uuid.uuid4(), "body": "Question body?" }
    create_data = {**defaults, **data}
    service = QuestionsService()
    return service.create_question(create_data)

def test_create_question():
    service = QuestionsService()
    request_data = {
        "author_id": uuid.uuid4(),
        "body": "What is the meaning of life?"
    }

    created_question = service.create_question(request_data)
    assert created_question.id is not None
    assert created_question.author_id == request_data["author_id"]
    assert created_question.body == request_data["body"]

def test_update_question():
    service = QuestionsService()
    question_id = uuid.uuid4()
    create_data = { "id": question_id }
    _create_question(create_data)

    update_data = { "author_id": uuid.uuid4(), "body": "Updated question body" }

    updated_question = service.update_question(question_id, update_data)
    assert updated_question.id == question_id
    assert updated_question.body == update_data["body"]

def test_get_question():
    service = QuestionsService()
    question_id = uuid.uuid4()  # Assuming this ID exists in your test database
    _create_question({"id": question_id})

    question = service.get_question(question_id)
    assert question.id == question_id
    assert question.author_id is not None
    assert question.body is not None

def test_get_questions():
    service = QuestionsService()
    filters = QuestionFilters(author_id=uuid.uuid4())
    page_size = 1
    page_number = 1

    page = service.get_questions(filters, page_number, page_size)
    assert isinstance(page, Page)
    assert len(page.content) <= page_size
    assert page.page == page_number
    assert all(isinstance(question, Question) for question in page.content)
