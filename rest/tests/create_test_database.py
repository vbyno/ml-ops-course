import os
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

DATABASE_NAME = os.getenv('POSTGRES_DB') + '_test'
# DATABASE_URL="postgresql://user:pass@localhost:5432/ml_ops_api_design"
DATABASE_URL = os.getenv('DATABASE_URL')

try:
    conn = psycopg2.connect(DATABASE_URL)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    cursor.execute(f"CREATE DATABASE {DATABASE_NAME}")
    cursor.close()
    conn.close()
except Exception as e:
    print(f"An error occurred: {e}")
