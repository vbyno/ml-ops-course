import uuid

from application.models import Page
from application.answer.answers_service import AnswersService, AnswerFilters
from application.answer.model.answer import Answer

def _create_answer(data):
    defaults = {
        "id": uuid.uuid4(),
        "author_id": uuid.uuid4(),
        "question_id": uuid.uuid4(),
        "body": "Answer body",
        "score": 3
    }
    create_data = {**defaults, **data}
    service = AnswersService()
    return service.create_answer(create_data)

def test_create_answer():
    service = AnswersService()
    request_data = {
        "id": uuid.uuid4(),
        "question_id": uuid.uuid4(),
        "author_id": uuid.uuid4(),
        "body": "What is the meaning of life?",
        "score": 3
    }

    created_answer = service.create_answer(request_data)
    assert created_answer.id is not None
    assert created_answer.author_id == request_data["author_id"]
    assert created_answer.body == request_data["body"]

def test_update_answer():
    service = AnswersService()
    id = uuid.uuid4()
    create_data = { "id": id }
    entity = _create_answer(create_data)

    update_data = {
        "author_id": uuid.uuid4(),
        "body": "Updated question body",
        "question_id": entity.question_id,
        "score": entity.score
    }

    updated = service.update_answer(id, update_data)
    assert updated.id == id
    assert updated.body == update_data["body"]

def test_get_answer():
    service = AnswersService()
    id = uuid.uuid4()
    _create_answer({"id": id})

    answer = service.get_answer(id)
    assert answer.id == id
    assert answer.author_id is not None
    assert answer.body is not None

def test_get_answers():
    service = AnswersService()
    author_id = uuid.uuid4()
    question_id = uuid.uuid4()
    filters = AnswerFilters(author_id=author_id, question_id=question_id)
    page_size = 1
    page_number = 1
    _create_answer({"question_id": question_id, "author_id": author_id})
    page = service.get_answers(filters, page_number, page_size)
    assert isinstance(page, Page)
    assert len(page.content) == page_size
    assert page.page == page_number
    assert all(isinstance(question, Answer) for question in page.content)
