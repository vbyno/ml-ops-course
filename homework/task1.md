# Task 1

Розширити пакет rest реалізацією. налаштуванами кластер в докер-компоуз для взаємодії з реальною базою та додати вольюм для перзістансу

# Development
```bash
pyenv virtualenv 3.11.4 python3.11-api-design
pyenv shell python3.11-api-design

flask db migrate -m "E"
flask db upgrade

APP_ENV=test flask db upgrade
pytest
```

Run docker-compose
```bash
cp .env.example .env
docker-compose up
```

## Questions API
Get /questions
[http://localhost:8000/questions](http://localhost:8000/questions)

```bash
curl 'http://localhost:8000/questions'

{
  "content": [],
  "page": null,
  "size": null,
  "total_pages": 0
}
```

Let's create a new question:
```bash
curl --location 'http://localhost:8000/questions' \
--header 'Content-Type: application/json' \
--data '{"author_id": "307ee604-54c9-4a4b-b457-846402a0badf", "body": "How are you?"}'

{
    "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
    "body": "How are you?",
    "id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"
}
```

Now questions list returns a collection containing this question:
```bash
curl 'http://localhost:8000/questions'
{
  "content": [
    {
      "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
      "body": "How are you?",
      "id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"
    }
  ],
  "page": null,
  "size": null,
  "total_pages": 1
}
```

Let's update question:
```bash
curl --location --request PUT 'http://localhost:8000/questions/f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977' \
--header 'Content-Type: application/json' \
--data '{"author_id": "307ee604-54c9-4a4b-b457-846402a0badf", "body": "How are you?(edited)"}'

{
  "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
  "body": "How are you?(edited)",
  "id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"
}
```

Now let's check if GET url returns updated question data
```bash
curl http://localhost:8000/questions/f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977
{
  "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
  "body": "How are you?(edited)",
  "id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"
}
```

And the collection:
```bash
curl http://localhost:8000/questions
{
  "content": [
    {
      "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
      "body": "How are you?(edited)",
      "id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"
    }
  ],
  "page": null,
  "size": null,
  "total_pages": 1
}
```

## Answers API
Answers CRU operations:
```bash
curl 'http://localhost:8000/answers'
{
  "content": [],
  "page": null,
  "size": null,
  "total_pages": 0
}


curl --location 'http://localhost:8000/answers' \
--header 'Content-Type: application/json' \
--data '{"author_id": "307ee604-54c9-4a4b-b457-846402a0badf", "body": "I am fine!", "score": 0, "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"}'

{
  "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
  "body": "I am fine!",
  "id": "6f65349d-ba90-4bca-ba9d-5197ecee302e",
  "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977",
  "score": 0
}

curl --location --request PUT 'http://localhost:8000/answers/6f65349d-ba90-4bca-ba9d-5197ecee302e' \
--header 'Content-Type: application/json' \
--data '{"author_id": "307ee604-54c9-4a4b-b457-846402a0badf", "body": "I am fine!", "score": 5, "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977"}'

{
  "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
  "body": "I am fine!",
  "id": "6f65349d-ba90-4bca-ba9d-5197ecee302e",
  "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977",
  "score": 5
}

curl 'http://localhost:8000/answers'
{
  "content": [
    {
      "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
      "body": "I am fine!",
      "id": "6f65349d-ba90-4bca-ba9d-5197ecee302e",
      "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977",
      "score": 5
    }
  ],
  "page": null,
  "size": null,
  "total_pages": 1
}

curl 'http://localhost:8000/answers/6f65349d-ba90-4bca-ba9d-5197ecee302e'
{
  "author_id": "307ee604-54c9-4a4b-b457-846402a0badf",
  "body": "I am fine!",
  "id": "6f65349d-ba90-4bca-ba9d-5197ecee302e",
  "question_id": "f50ac1d4-a8d0-4b6e-88bb-3c67aaa4c977",
  "score": 5
}
```
